<div class="left_col scroll-view">
  <div class="navbar nav_title" style="border: 0;">
    <a href="index.html" class="site_title"><i class="fa fa-bank"></i> <span>LIbrary</span></a>
  </div>
  <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="{{ asset('1.png') }}" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>Aditya</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a href="/home"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="{{ route('siswa.index') }}"><i class="fa fa-users"></i>Siswa</a></li>
            <li><a href="{{ route('book_category.index') }}"><i class="fa fa-list"></i>Kategory</a></li>
            <li><a href="{{ route('books.index') }}"><i class="fa fa-book"></i>Buku</a></li>
            <li><a href="{{ route('borrow.index') }}"><i class="fa fa-recycle"></i>Pinjam</a></li>
        </ul>
    </div>
  </div>
</div>
      <!-- /sidebar menu -->