@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Tambah Data Peminjaman</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('borrow.store') }}" method="post">
            @csrf
                <label for="siswa">Nama Siswa</label>
                  <select class="form-control" id="siswa" name="siswa">
                                <option>chose...</option>
                                @foreach ($siswa as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                <br>
                <label for="books">Judul Buku</label>
                <select class="form-control" id="books" name="books">
                                <option>chose...</option>
                                @foreach ($books as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                    
                <br>
                <div class="from-group">
                    <label for="start">Tanggal Peminjaman Buku</label>
                    <input type="date" name="start" class="form-control"
                     id="start">
                </div>
                <br>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
        <!-- end for validations -->
    </div>
</div>
@endsection

                           