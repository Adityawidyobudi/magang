@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Peminjaman</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('borrow.update', $borrow->id) }}" method="post">
            @csrf
            @method('put')
                <label for="siswa">Nama Siswa</label>
                  <select class="form-control" id="siswa" name="siswa">
                                <option>chose...</option>
                                @foreach ($siswa as $item)
                                <option value="{{ $item->id }}"{{ $borrow->siswa_id == $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                <br>
                <label for="books">Judul Buku</label>
                <select class="form-control" id="books" name="books">
                                <option>chose...</option>
                                @foreach ($books as $item)
                                <option value="{{ $item->id }}"{{ $borrow->book_id == $item->id ? 'selected' : ''}}>{{ $item->title }}</option>
                                @endforeach
                            </select>
                    
                <br>
                <div class="from-group">
                    <label for="start">Tanggal Pinjam</label>
                    <input type="date" name="start" class="form-control"
                     id="start" value="{{ $borrow->start }}">
                </div>

                <br>
                <div class="from-group">
                    <label for="return">Tanggal Kembali<small id="kembali"></label></small>
                    <input type="date" name="return" class="form-control"
                     id="return">
                </div>

                <br>
                 <label class="mr-4">Status</label><br>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status" id="status1" value="dipinjam" class="custom-control-input" {{ $borrow->status == 'dipinjam' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="status1">dipinjam</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status" id="status2" value="dikembalikan" class="custom-control-input" {{ $borrow->status == 'dikembalikan' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="status2">dikembalikan</label>
                </div>

                <br>
                <div class="from-group">
                    <label for="denda">Denda</label>
                    <input type="text" class="form-control" name="denda" 
                     id="denda" readonly>
                </div>
                <br>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
        <!-- end for validations -->
    </div>
</div>
@endsection

@push('script')
 <script>
    $('#start, #return').change(()=>{
        var Pinjam = $('#start').val()
        var Kembali = $('#return').val()
        // cek jika input pinjam dan kembali tidak kosong
        if ( Date.parse(Pinjam) && Date.parse(Kembali) )
        {
                Pinjam = new Date(Pinjam)
                Kembali = new Date(Kembali)

            // membuat tanggal deadline
            var deadline = Pinjam.setDate(Pinjam.getDate() + 7)

            //menghitung selisih hari dan denda
            var selisih = (Kembali - deadline) / (1000 * 3600 * 24)
            var denda = selisih > 0 ? (selisih*500) : 0;

            $('#denda').val(denda)

            //membuat fungsi data tunggal deadline untuk ditampilkan
            var convertDeadline = new Date(deadline)
            var formattedDate = ('0' + convertDeadline.getDate()).slice(-2);
            var formattedMonth = ('0' + (convertDeadline.getMonth() + 1)).slice(-2);
            var formattedYear = convertDeadline.getFullYear().toString().substr(2,2);
            var dateString = formattedMonth + '/' + formattedDate + '/' + formattedYear;

            // menampilkan deadline
            $('#kembali').text( '(Deadline : '+dateString+' )' )

        } 
        else {
            $('#kembali').text( '(Deadline : -)' ) 
            $('#denda').val(0)
        }
    })
</script>
@endpush

                           