@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                Tambah Data Siswa
                </div>

                <div class="card-body">
                        <form action="/siswa/store" method="post">
                            @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="nama">Nama</label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Siswa">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="nis">Nis</label>
                                        <input type="number" name="nis" class="form-control" id="nis" placeholder="Masukkan Nis Siswa">
                                    </div>

                                    <label class="mr-4">Jenis Kelamin</label>
                                    <div class="form-group">
                                        <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="gender" id="gender1" value="laki-laki" class="custom-control-input" >
                                        <label class="custom-control-label" for="gender1">Laki-Laki</label>
                                        </div>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="gender" id="gender2" value="perempuan" class="custom-control-input" >
                                        <label class="custom-control-label" for="gender2">Perempuan</label>
                                        </div>
                                    </div>
                                    <div calss="form-group">
                                        <label for="tanggal_lahir">Tanggal Lahir</label>
                                        <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" placeholder="Pilih Tanggal Lahir">
                                    </div>

                                <button type="submit" class="btn btn-danger">Simpan</button>
                                <a href="/siswa" class="btn btn-warning">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
